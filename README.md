# EmRAVE community-database

This project contains a snowflake docker image for the staging community database.

# prepare log :de:


### Image laden

```bash
PS C:\\Users\\hhaus\> docker pull mysql/mysql-server:latest

latest: Pulling from mysql/mysql-server

0e690826fc6e: Pull complete

0e6c49086d52: Pull complete

862ba7a26325: Pull complete

7731c802ed08: Pull complete

Digest:
sha256:a82ff720911b2fd40a425fd7141f75d7c68fb9815ec3e5a5a881a8eb49677087

Status: Downloaded newer image for mysql/mysql-server:latest

docker.io/mysql/mysql-server:latest

```
### 

###  Image Starten und vorbereiten

```bash
PS C:\\Users\\hhaus\> docker run \--name=mysql01 -p 3306:3306 -d
mysql/mysql-server:latest

b5af486dec35f8a275311107ad04350eacf4bbacf96ee162e58d23eb62ea68b7

PS C:\\Users\\hhaus\> docker logs mysql01

\[Entrypoint\] MySQL Docker Image 8.0.20-1.1.16

\[Entrypoint\] No password option specified for new database.

\[Entrypoint\] A random onetime password will be generated.

\[Entrypoint\] Initializing database

2020-06-27T13:27:01.171733Z 0 \[System\] \[MY-013169\] \[Server\]
/usr/sbin/mysqld (mysqld 8.0.20) initializing of server in progress as
process 20

2020-06-27T13:27:01.175337Z 1 \[System\] \[MY-013576\] \[InnoDB\] InnoDB
initialization has started.

2020-06-27T13:27:01.532882Z 1 \[System\] \[MY-013577\] \[InnoDB\] InnoDB
initialization has ended.

2020-06-27T13:27:02.435243Z 6 \[Warning\] \[MY-010453\] \[Server\]
root\@localhost is created with an empty password ! Please consider
switching off the \--initialize-insecure option.

\[Entrypoint\] Database initialized

2020-06-27T13:27:05.045085Z 0 \[System\] \[MY-010116\] \[Server\]
/usr/sbin/mysqld (mysqld 8.0.20) starting as process 65

2020-06-27T13:27:05.058472Z 1 \[System\] \[MY-013576\] \[InnoDB\] InnoDB
initialization has started.

2020-06-27T13:27:05.211825Z 1 \[System\] \[MY-013577\] \[InnoDB\] InnoDB
initialization has ended.

2020-06-27T13:27:05.283698Z 0 \[System\] \[MY-011323\] \[Server\] X
Plugin ready for connections. Socket: \'/var/run/mysqld/mysqlx.sock\'

2020-06-27T13:27:05.395719Z 0 \[Warning\] \[MY-010068\] \[Server\] CA
certificate ca.pem is self signed.

2020-06-27T13:27:05.410951Z 0 \[System\] \[MY-010931\] \[Server\]
/usr/sbin/mysqld: ready for connections. Version: \'8.0.20\' socket:
\'/var/lib/mysql/mysql.sock\' port: 0 MySQL Community Server - GPL.

Warning: Unable to load \'/usr/share/zoneinfo/iso3166.tab\' as time
zone. Skipping it.

Warning: Unable to load \'/usr/share/zoneinfo/leapseconds\' as time
zone. Skipping it.

Warning: Unable to load \'/usr/share/zoneinfo/tzdata.zi\' as time zone.
Skipping it.

Warning: Unable to load \'/usr/share/zoneinfo/zone.tab\' as time zone.
Skipping it.

Warning: Unable to load \'/usr/share/zoneinfo/zone1970.tab\' as time
zone. Skipping it.

\[Entrypoint\] GENERATED ROOT PASSWORD: radruf(URQeSubeh.id4gAb\[Uv4

\[Entrypoint\] ignoring /docker-entrypoint-initdb.d/\*

2020-06-27T13:27:06.881097Z 10 \[System\] \[MY-013172\] \[Server\]
Received SHUTDOWN from user root. Shutting down mysqld (Version:
8.0.20).

2020-06-27T13:27:08.827076Z 0 \[System\] \[MY-010910\] \[Server\]
/usr/sbin/mysqld: Shutdown complete (mysqld 8.0.20) MySQL Community
Server - GPL.

\[Entrypoint\] Server shut down

\[Entrypoint\] Setting root user as expired. Password will need to be
changed before database can be used.

\[Entrypoint\] MySQL init process done. Ready for start up.

\[Entrypoint\] Starting MySQL 8.0.20-1.1.16

2020-06-27T13:27:09.085162Z 0 \[System\] \[MY-010116\] \[Server\]
/usr/sbin/mysqld (mysqld 8.0.20) starting as process 1

2020-06-27T13:27:09.091224Z 1 \[System\] \[MY-013576\] \[InnoDB\] InnoDB
initialization has started.

2020-06-27T13:27:09.243039Z 1 \[System\] \[MY-013577\] \[InnoDB\] InnoDB
initialization has ended.

2020-06-27T13:27:09.315535Z 0 \[System\] \[MY-011323\] \[Server\] X
Plugin ready for connections. Socket: \'/var/run/mysqld/mysqlx.sock\'
bind-address: \'::\' port: 33060

2020-06-27T13:27:09.395376Z 0 \[Warning\] \[MY-010068\] \[Server\] CA
certificate ca.pem is self signed.

2020-06-27T13:27:09.416418Z 0 \[System\] \[MY-010931\] \[Server\]
/usr/sbin/mysqld: ready for connections. Version: \'8.0.20\' socket:
\'/var/lib/mysql/mysql.sock\' port: 3306 MySQL Community Server - GPL.

PS C:\\Users\\hhaus\> docker exec -it mysql01 mysql -uroot -p

Enter password:

Welcome to the MySQL monitor. Commands end with ; or \\g.

Your MySQL connection id is 18

Server version: 8.0.20 MySQL Community Server - GPL

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights
reserved.

Oracle is a registered trademark of Oracle Corporation and/or its

affiliates. Other names may be trademarks of their respective

owners.

Type \'help;\' or \'\\h\' for help. Type \'\\c\' to clear the current
input statement.

mysql\> CREATE DATABASE \[IF NOT EXISTS\] os1

-\> ;

ERROR 1064 (42000): You have an error in your SQL syntax; check the
manual that corresponds to your MySQL server version for the right
syntax to use near \'\[IF NOT EXISTS\] os1\' at line 1

mysql\> CREATE DATABASE os1;

Query OK, 1 row affected (0.01 sec)

mysql\> exit

Bye

```



### DB User für OpenSocial anlegen

```bash
PS C:\\Users\\hhaus\> docker exec -it mysql01 mysql -uroot -p

Enter password:

Welcome to the MySQL monitor. Commands end with ; or \\g.

Your MySQL connection id is 85

Server version: 8.0.20 MySQL Community Server - GPL

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights
reserved.

Oracle is a registered trademark of Oracle Corporation and/or its

affiliates. Other names may be trademarks of their respective

owners.

Type \'help;\' or \'\\h\' for help. Type \'\\c\' to clear the current
input statement.

mysql\> CREATE USER \'opensocial\'@\'localhost\' IDENTIFIED BY
\'password\';

Query OK, 0 rows affected (0.01 sec)

mysql\> GRANT ALL PRIVILEGES ON \*.\* TO \'opensocial\'@\'localhost\'
WITH GRANT OPTION;

Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql\> CREATE USER \'opensocial\'@\'%\' IDENTIFIED BY \'password\';

Query OK, 0 rows affected (0.00 sec)

mysql\> GRANT ALL PRIVILEGES ON \*.\* TO \'opensocial\'@\'%\' WITH GRANT
OPTION;

Query OK, 0 rows affected (0.01 sec)

mysql\> FLUSH PRIVILEGES;

Query OK, 0 rows affected (0.00 sec)

-   Anpassung Login-Methode für PHP

-   Siehe
    <https://stackoverflow.com/questions/52364415/php-with-mysql-8-0-error-the-server-requested-authentication-method-unknown-to>

mysql\> ALTER USER \'opensocial\'@\'%\' IDENTIFIED WITH
mysql\_native\_password

-\> BY \'password\';

Query OK, 0 rows affected (0.01 sec)

mysql\>
```



Commit images and push to registry
----------------------------------

```bash
PS C:\\Users\\hhaus\> docker ps

CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES

b5af486dec35 mysql/mysql-server:latest \"/entrypoint.sh mysq...\" 2
hours ago Up 2 hours (healthy) 0.0.0.0:3306-\>3306/tcp, 33060/tcp
mysql01

1bc3161b87f1 goalgorilla/open\_social\_docker \"docker-php-entrypoi...\"
2 weeks ago Up 2 hours 0.0.0.0:80-\>80/tcp objective\_yalow


PS C:\\Users\\hhaus\> docker login registry.gitlab.com

Username: h.muc.hauschild\@googlemail.com

Password:

Login Succeeded



PS C:\\Users\\hhaus\> docker commit mysql01
registry.gitlab.com/emergency-rave/services/community-database

sha256:97d7b6516907601a5c05275311f74074390ff018bbd1b8b06f900dc773baeb20

PS C:\\Users\\hhaus\> docker push
registry.gitlab.com/emergency-rave/services/community-database

The push refers to repository
\[registry.gitlab.com/emergency-rave/services/community-database\]

00f5780a7022: Pushed

9f0078eb6299: Pushed

bc9aebdeb35d: Pushed

cab47d83aeb3: Pushing \[=============================================\>
\] 237.4MB/261.6MB

bc198e3a2f79: Pushing \[===================================\> \]
85.47MB/119.9MB

```